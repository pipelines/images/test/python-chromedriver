FROM docker.io/library/debian:stable-slim

COPY chrome.gpg /etc/apt/trusted.gpg.d/google-chrome.gpg
COPY chrome.list /etc/apt/sources.list.d/google-chrome.list
COPY --chmod=0755 install-chrome-driver /usr/local/bin/

RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -q -y install --no-install-recommends \
        build-essential python3 python3-dev python3-pip tox git brotli \
        google-chrome-stable npm curl jq unzip && \
    /usr/local/bin/install-chrome-driver && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*

