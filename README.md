Python/Chromedriver test container image
===

Container image with *Tox* and a Python dev environment, based
on Debian stable. The image also includes Selenium and Chromedriver,
a headless browser test harness.

